function formulario(){
	var formData = new FormData();
    formData.append("name",$('#contacts-first-name').val());
    formData.append("last_name",$('#contacts-last-name').val());
    formData.append("email",$('#contacts-email').val());
    formData.append("phone",$('#contacts-phone').val());
	formData.append("message",$('#contacts-message').val());
  
    var URL_BASE=$("#base_url").val();
    var screen = $('#loading-screen');
    screen.fadeIn();
    
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: URL_BASE+"/store",
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false ,
        success:function(data) {
            screen.fadeOut();
            Swal.fire({
                title: "Envio de email!",
                text: data.msg,
                icon: "success",
                button: "ok",
            });
            if($('#contacts-first-name').val()==""){
                console.log(data.responseJSON.x.name);
            $("#contacts-first-name").addClass("is-invalid");
            $("#nameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.name);
        }
        else{
            $("#name").removeClass("is-invalid");
            $("#nameFeedback").removeClass("invalid-feedback").empty().html("");
        }
         if($('#contacts-last-name').val()==""){
            $("#contacts-last-name").addClass("is-invalid");
            $("#lastnameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.last_name);
        }
        else{
            $("#contacts-last-name").removeClass("is-invalid");
            $("#lastnameFeedback").removeClass("invalid-feedback").empty().html("");
        }
        if($('#contacts-email').val()==""){
            $("#contacts-email").addClass("is-invalid");
            $("#emailFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.email);
        }
        else{
            $("#contacts-email").removeClass("is-invalid");
            $("#emailFeedback").removeClass("invalid-feedback").empty().html("");
        }
        if($('#contacts-phone').val()==""){
            $("#contacts-phone").addClass("is-invalid");
            $("#phoneFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.phone);
        }
        else{
            $("#contacts-phone").removeClass("is-invalid");
            $("#phoneFeedback").removeClass("invalid-feedback").empty().html("");
        }
        if($('#contacts-message').val()==""){
            $("#contacts-message").addClass("is-invalid");
            $("#messageFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.message);
        }
        else{
            $("#contacts-message").removeClass("is-invalid");
            $("#messageFeedback").removeClass("invalid-feedback").empty().html("");
        }
            $('#contacts-first-name').val('');
            $('#contacts-last-name').val('');
            $('#contacts-email').val('');
            $('#contacts-phone').val('');
            $('#contacts-message').val('');
  
        }      
    }).done(function() {
    
    }).fail(function(data) {
        screen.fadeOut();
         if($('#contacts-first-name').val()==""){
                console.log(data.responseJSON.x.name);
            $("#contacts-first-name").addClass("is-invalid");
            $("#nameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.name);
        }
        else{
            if(data.responseJSON.x.name!="" && data.responseJSON.x.name!=undefined )
            {
              $("#contacts-first-name").addClass("is-invalid");
              $("#nameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.name);
            }
            else{
                $("#contacts-first-name").removeClass("is-invalid");  
                $("#nameFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        if($('#contacts-last-name').val()==""){
            $("#contacts-last-name").addClass("is-invalid");
            $("#lastnameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.last_name);
        }
        else{
            if(data.responseJSON.x.last_name!="" && data.responseJSON.x.last_name!=undefined)
            {
              $("#contacts-last-name").addClass("is-invalid");
              $("#lastnameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.last_name);
            }
            else{
                $("#contacts-last-name").removeClass("is-invalid");  
                $("#lastnameFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        if($('#contacts-email').val()==""){
            $("#contacts-email").addClass("is-invalid");
            $("#emailFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.email);
        }
        else{
             if(data.responseJSON.x.email!="" && data.responseJSON.x.email!=undefined)
            {
              $("#contacts-email").addClass("is-invalid");
              $("#emailFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.email);
            }else{
                $("#contacts-email").removeClass("is-invalid");
                $("#emailFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        if($('#contacts-phone').val()==""){
            $("#contacts-phone").addClass("is-invalid");
            $("#phoneFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.phone);
        }
        else{
            if(data.responseJSON.x.phone!="" && $('#contacts-phone').val().length<9)
            {
              $("#contacts-phone").addClass("is-invalid");
              $("#phoneFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.phone);
            }
            else{    
                $("#contacts-phone").removeClass("is-invalid");
                $("#phoneFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        if($('#contacts-message').val()==""){
            $("#contacts-message").addClass("is-invalid");
            $("#messageFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.message);
        }
        else{
            $("#contacts-message").removeClass("is-invalid");
            $("#messageFeedback").removeClass("invalid-feedback").empty().html("");
        }
       
        Swal.fire({
		title: "Envio de email!",
		text: "No se pudo enviar el email! "+ data.responseJSON.errors,
		icon: "warning",
		button: "ok",
		});
    
   	}).always(function() {
 
    
    });
}
function Cerrar(){
  $("#myModal").modal("hide");
}