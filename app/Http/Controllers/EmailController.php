<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Redirect;
class EmailController extends Controller
{
    public function index(){
        return view('welcome');
    }
    public function enviarmail(Request $request)
    {
   
        $rules=[
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'responsable' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email',
            'phone'=>'required|min:9|',
            'message'=>'required',
        ];

        $mensajes=[
            'name.required'=>'El nombre de la empresas es obligatorio',
            'name.regex'=>'El nombre de la empresas solo puede tener letras',
            'responsable.required'=>'El responsable es obligatorio',
            'responsable.regex'=>'El responsable solo puede tener letras',
            'email.required'=>'El correo electronico es obligatorio',
            'email.email'=>'El correo electronico debe tener un  formato valido',
            'message.required'=>'El mensaje es  obligatorio',
            'phone.required'=>'El telefono de contacto es  obligatorio',
            'phone.min'=>'El telefono debe tener minimo 9 digitos',
            'phone.regex'=>'no tiene un formato valido para telefono',
        ];
        $validator = \Validator::make($request->all(), $rules,$mensajes);
        if ($validator->fails()) {
            $notify['msg'] = 'Error al enviar formulario de contacto';
            $notify['type'] = 'danger';
            /*return Redirect::back()->withErrors($validator)->with(compact('notify'))->withInput($request->all());*/
            return response()->json(array('success'=>false,
                'errors'=>"Debe llenar los campos obligatorios",
                "x"=>$validator->errors()
            ),
            400);
        }
        try
        {

            $email="contacto@tecnobravo.com"; 
            $from= "formularioweb@tecnobravo.com";
            $recibo="contacto@tecnobravo.com";
            
            $subject="Solicitud de informacion realizada desde el formulario de contactos de Kit Digital";
            $name=$request->name;
            $responsable=$request->responsable;
            $phone=$request->phone;
            $msg=$request->message;
            $contacto=$request->email;
            $MAIL_USERNAME=env('MAIL_USERNAME');
            Mail::send('mail/contacto', 
            [
                'name' =>$name,
                'responsable' =>$responsable,
                'phone'=>$phone,
                'contacto'=>$contacto,
                'msg' =>$msg
            ], 
            function ($mail) use($email,$recibo,$from,$subject) {
                $mail->from($from, 'Kit Digital');
                $mail->to($email);
                $mail->bcc($recibo);
                $mail->subject($subject);
            });
            return   response()->json([
                'success'=>true,
                'msg'=>"Se envio el correo con exito a Kit Digital le estaremos contactando",
            ],200);
        }
        catch (Exception $e)
        {
            \Log::info('Error creando el Medio de contacto: '.$e);
            return \Response::json(['created' => false], 500);
        }
       
  }
}
