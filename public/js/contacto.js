function formulario(){
	var formData = new FormData();
    formData.append("name",$('#name').val());
    formData.append("responsable",$('#responsable').val());
    formData.append("email",$('#email').val());
    formData.append("phone",$('#phone').val());
	formData.append("message",$('#message').val());
  
    var URL_BASE=$("#base_url").val();
    var screen = $('#loading-screen');
    screen.fadeIn();
    
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: URL_BASE+"/store",
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false ,
        success:function(data) {
            screen.fadeOut();
            Swal.fire({
                title: "Envio de email!",
                text: data.msg,
                icon: "success",
                button: "ok",
            });
            if($('#name').val()==""){
                console.log(data.responseJSON.x.name);
            $("#name").addClass("is-invalid");
            $("#nameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.name);
        }
        else{
            $("#name").removeClass("is-invalid");
            $("#nameFeedback").removeClass("invalid-feedback").empty().html("");
        }
         if($('#responsable').val()==""){
            $("#responsable").addClass("is-invalid");
            $("#responsableFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.responsable);
        }
        else{
            $("#responsable").removeClass("is-invalid");
            $("#responsableFeedback").removeClass("invalid-feedback").empty().html("");
        }
        if($('#email').val()==""){
            $("#email").addClass("is-invalid");
            $("#emailFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.email);
        }
        else{
            $("#email").removeClass("is-invalid");
            $("#emailFeedback").removeClass("invalid-feedback").empty().html("");
        }
        if($('#phone').val()==""){
            $("#phone").addClass("is-invalid");
            $("#phoneFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.phone);
        }
        else{
            $("#phone").removeClass("is-invalid");
            $("#phoneFeedback").removeClass("invalid-feedback").empty().html("");
        }
        if($('#message').val()==""){
            $("#message").addClass("is-invalid");
            $("#messageFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.message);
        }
        else{
            $("#message").removeClass("is-invalid");
            $("#messageFeedback").removeClass("invalid-feedback").empty().html("");
        }
            $('#name').val('');
            $('#responsable').val('');
            $('#email').val('');
            $('#phone').val('');
            $('#message').val('');
  
        }      
    }).done(function() {
    
    }).fail(function(data) {
        screen.fadeOut();
         if($('#name').val()==""){
                console.log(data.responseJSON.x.name);
            $("#name").addClass("is-invalid");
            $("#nameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.name);
        }
        else{
            if(data.responseJSON.x.name!="" && data.responseJSON.x.name!=undefined )
            {
              $("#name").addClass("is-invalid");
              $("#nameFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.name);
            }
            else{
                $("#name").removeClass("is-invalid");  
                $("#nameFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        console.log(data.responseJSON.x.responsable);
        if($('#responsable').val()==""){
            $("#responsable").addClass("is-invalid");
            $("#responsableFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.responsable);
        }
        else{
            if(data.responseJSON.x.responsable!="" && data.responseJSON.x.responsable!=undefined)
            {
              $("#responsable").addClass("is-invalid");
              $("#responsableFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.responsable);
            }
            else{
                $("#responsable").removeClass("is-invalid");  
                $("#responsableFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        if($('#email').val()==""){
            $("#email").addClass("is-invalid");
            $("#emailFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.email);
        }
        else{
             if(data.responseJSON.x.email!="" && data.responseJSON.x.email!=undefined)
            {
              $("#email").addClass("is-invalid");
              $("#emailFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.email);
            }else{
                $("#email").removeClass("is-invalid");
                $("#emailFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        if($('#phone').val()==""){
            $("#phone").addClass("is-invalid");
            $("#phoneFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.phone);
        }
        else{
            if(data.responseJSON.x.phone!="" && $('#phone').val().length<9)
            {
              $("#phone").addClass("is-invalid");
              $("#phoneFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.phone);
            }
            else{    
                $("#phone").removeClass("is-invalid");
                $("#phoneFeedback").removeClass("invalid-feedback").empty().html("");
            }
        }
        if($('#message').val()==""){
            $("#message").addClass("is-invalid");
            $("#messageFeedback").addClass("invalid-feedback").empty().html(data.responseJSON.x.message);
        }
        else{
            $("#message").removeClass("is-invalid");
            $("#messageFeedback").removeClass("invalid-feedback").empty().html("");
        }
       
        Swal.fire({
		title: "Envio de email!",
		text: "No se pudo enviar el email! "+ data.responseJSON.errors,
		icon: "warning",
		button: "ok",
		});
    
   	}).always(function() {
 
    
    });
}
function Cerrar(){
  $("#myModal").modal("hide");
}