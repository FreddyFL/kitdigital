<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>TecnoBravo - Kit Digital</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- css link -->
    <!-- favicon icon -->
    <link rel="icon" type="images/png" href="images/favicon.png" />
    <!-- bootstrap min css -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- owl carousel min css -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- all min css -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <!-- animated min css -->
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <!-- slicknav min css -->
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <!-- css link -->
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
</head>

<body>

    <!-- proloader start -->
    <!-- <div class="loader-bg">
        <h3>Cargando...</h3>
      <div class="loader">
          <span></span>
      </div>
    </div>-->
    <!-- proloader end -->
    <!-- hero area start -->
    
    <section class="hero-area" id="home">
        <div class="col-lg-5 hero-img-col wow animate__animated animate__fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
            <img src="images/hero-img.png" alt="hero image">
        </div>
        <div class="container">
            <div class="row hero-area-row">
                <div class="col-lg-6 hero-content-col">
                    <div class="hero-content-area"><div class="footer-logo">
                                <a href="#"><img src="images/logo.png" alt="footer logo"></a>
                            </div>
                        <h1 class="wow animate__animated animate__fadeInRight" data-wow-duration="2s" data-wow-delay="0.2s"><span></span>Agentes Digitalizadores</h1>
                        <p class="wow animate__animated animate__fadeInRight" data-wow-duration="2s" data-wow-delay="0.2s">Te ayudamos a obtener tu KIT DIGITAL. Hasta 12.000€ de ayudas del gobierno para digitalizar tu negocio.</p>
                        <div class="hero-btn-area wow animate__animated animate__fadeInRight" data-wow-duration="2.5s" data-wow-delay="0.2.5s">
                            <a href="tel:+34977552258" class="button-btn">Llámanos - 977 55 22 58</a>
                            <a href="#contact" class="button-btn transparent-btn">Rellenar Formulario</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-shape"></div>
    </section>
    <!-- hero area end -->
    <!-- services area start -->
    <section id="services-area" class="services-area">
        <div class="services-shape-1"></div>
        <div class="container">
            <div class="row services-top-row">
                <div class="col-lg-5 services-top-left-col">
                    <div class="section-title wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h2><span>¿Qué Es</span>El Kit Digital?</h2>
                    </div>
                </div>
                <div class="col-lg-7 align-self-center services-top-right-col wow animate__animated animate__zoomIn" data-wow-duration="1s">
                    <p>El programa de ayudas Kit Digital es un programa de ayudas dotado de 3.067 millones de euros, financiado por la Unión Europea a través de los fondos Next Generation EU, cuyo objetivo es conseguir la digitalización de Pymes y autónomos. Está enmarcado en el Plan de Recuperación, Transformación y Resiliencia, la agenda España Digital 2025 y el Plan de Digitalización de Pymes 2021-2025. </p>
                </div>
            </div>
            
            <div class="section-title">
                        <h2>¿Quién Puede Solicitar El Kit Digital?</h2>
                        <br>
                    </div>
            
            <div class="row services-item-row">
                <div class="col-md-6 col-lg-4 services-item-col">
                    <div class="services-single-item wow animate__animated animate__fadeInUp" data-wow-duration="1.5s">
                        <div class="image"><img src="images/services-item-img-1.png" alt="building image"></div>
                        <h3>PYMES Entre 0 y Menos 3 Empleados</h3>
                        <p>Pueden optar a este segmento todas las pequeñas empresas o microempresas que tengan entre 0 y 2 trabajadores empleados o cualquier persona en situación de autoempleo (autónomo)  y que cumplan con el Nivel de Madurez Digital.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 services-item-col">
                    <div class="services-single-item active wow animate__animated animate__fadeInUp" data-wow-duration="1.9s">
                        <div class="image"><img src="images/services-item-img-5.png" alt="increase image"></div>
                        <h3>PYMES Entre 3 y Menos De 10 Empleados</h3>
                        <p>Pueden optar a este segmento todas las pequeñas empresas o microempresas que tengan de 3 a 9 trabajadores empleados o cualquier persona en situación de autoempleo (autónomo) y que cumplan con el Nivel de Madurez Digital. </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 services-item-col">
                    <div class="services-single-item wow animate__animated animate__fadeInUp" data-wow-duration="2s">
                        <div class="image"><img src="images/services-item-img-2.png" alt="seo image"></div>
                        <h3>PYMES Entre 10 y 50 Empleados </h3>
                        <p>Pueden optar a este segmento todas las pequeñas empresas que tengan de 10 a 49 trabajadores empleados o cualquier persona en situación de autoempleo (autónomo) y que cumplan con el Nivel de Madurez Digital.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="services-shape-3"></div>
        <div class="services-shape-2"></div>
    </section>
    <!-- services area end -->
    <!-- boots area start -->
    <section class="boosts-area">
        <div class="container">
            <div class="row boosts-top-row">
                <div class="col-lg-6 boosts-content-col wow animate__animated animate__fadeInLeft" data-wow-duration="1.5s">
                    <div class="section-title">
                        <h2><span>¿Por Qué Confiar En</span> TecnoBravo?</h2>
                    </div>
                    <p>En TecnoBravo, desde el año 2000, trabajamos para dar soluciones tecnológicas para empresas. Sabemos que una buena infraestructura informática es un factor decisivo en términos competitivos: conlleva la reducción de costes y el aumento de la productividad. Las nuevas tecnologías nos acercan más y permiten, también, ofrecer unos servicios más personalizados, contribuyendo a potenciar el desarrollo de su empresa. A esto nos dedicamos: Damos soluciones para mejorar el rendimiento a través de la optimización de los recursos informáticos.</p>
                        <br>
      
                </div>
                <div class="col-lg-6 image-area align-self-center wow animate__animated animate__fadeInRight" data-wow-duration="2s">
                    <img src="images/boosts-area-img.png" alt="boosts area image">
                </div>
            </div>
        </div>
    </section>
    <!-- boots area end -->
    <!-- flexible area start -->
    <section id="pricing-area" class="flexible-area">
        <div class="flexible-shape-1"></div>
        <div class="container">
            <div class="row flexible-top-row wow animate__animated animate__zoomIn" data-wow-duration="1s">
                <div class="col-12 flexible-top-content-col">
                    <div class="section-title">
                        <h2><span>¿A Qué Está Destinada La Subvención?</span>Catálogo De Servicios</h2>
                    </div>
                </div>
            </div>
            <div class="row flexible-price-row wrapper-full" id="monthly">
                <!-- monthly price list content -->
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Diseño y Desarrollo Web</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Gastos profesionales de diseño, creación o implantación de páginas web corporativas sin portal de ventas.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Comercio Electrónico</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Gasto subvencionable para creación de tiendas online para facilitar el proceso de compra y resaltar los productos al máximo.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Redes Sociales</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Gastos subvencionables para la creación, diseño y puesta en marcha de planes y campañas en redes sociales.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Gestión de cliente CRM</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Ayuda para la creación, digitalización y optimización de procesos para la gestión de las relaciones con tus clientes.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Oficinas virtuales</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Ayudas para la formación y el uso de herramientas y servicios de oficinas virtuales y cloud.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Gestión de procesos</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Gastos subvencionables para la creación y gestión de los procesos productivos dentro de las empresas.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Factura electrónica </h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Subvenciones para la generación, formación y gestión de herramientas de facturas electrónicas.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Comunicaciones seguras</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Ayudas para comunicaciones seguras en el entorno productivo empresarial y desarrollo de labores comerciales.</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 flexible-price-item">
                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h4>Ciberseguridad online</h4>
                        <ul class="flexible-price-lsit">
                            <li><a href="#"><i class="fa fa-check-square" aria-hidden="true"></i>Gastos subvencionables para crear entornos seguros en las empresas con ciberseguridad online.</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="flexible-shape-2"></div>
    </section>
    <!-- flexible area end -->
    <!--  team area start -->
    <section id="team-area" class="our-team-area">
        <div class="container">
            <div class="row team-top-row">
                <div class="col-12 team-top-content-col">
                    <div class="section-title wow animate__animated animate__zoomIn" data-wow-duration="1s">
                        <h2><span>¿En qué te puede ayudar </span>el Kit Digital Para Hostelería?</h2>
                        <br>
                        <p>El kit Digital para hostelería puede ayudarte en la gestión de procesos en el restaurante (digitalizar la operativa de tu negocio en lo que se refiere a gestión del catálogo, precios, atributos, stock…), gestión de pedidos y la optimización de la información para convertirla en rentable para tu negocio.</p>
                    </div>
                    
                    <div class="container newsletter-container">
                        <div class="row newsletter-row">
                            <div class="col-lg-6 image-col wow animate__animated animate__fadeInLeft" data-wow-duration="1s">
                                <div class="image-area">
                                    <img src="images/newsletter-img.png" alt="newsletter image">
                                </div>
                            </div>
                            <div class="col-lg-5 newsletter-content-col align-self-center  wow animate__animated animate__fadeInRight" data-wow-duration="2s">
                                <div class="section-title">
                                    <h3><span></span> Soluciones en tu restaurante</h3>
                                </div>
                                <div>
                                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                                    <ul class="flexible-price-lsit">
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> TPV (Terminal Punto de Venta).</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Comandero o Telecomanda.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Monitor de cocina – KDS</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Cajones Portamonedas Inteligentes.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Pasarelas de pago.</p></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container newsletter-container">
                        <div class="row newsletter-row">
                            <div class="col-lg-5 newsletter-content-col align-self-center  wow animate__animated animate__fadeInRight" data-wow-duration="2s">
                                <div class="section-title">
                                    <h3><span></span> Soluciones de Administración y gestión</h3>
                                </div>
                                <div>
                                    <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                                    <ul class="flexible-price-lsit">
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Back Office.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Datos en tiempo real.</li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Compras: stock y almacén.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Recursos humanos.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Centralización de datos.</p></li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 image-col wow animate__animated animate__fadeInLeft" data-wow-duration="1s">
                                <div class="image-area">
                                    <img src="images/newsletter-img2.png" alt="newsletter image">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container newsletter-container">
                        <div class="row newsletter-row">
                            <div class="col-lg-6 image-col wow animate__animated animate__fadeInLeft" data-wow-duration="1s">
                                <div class="image-area">
                                    <img src="images/newsletter-img1.png" alt="newsletter image">
                                </div>
                            </div>
                        <div class="col-lg-5 newsletter-content-col align-self-center  wow animate__animated animate__fadeInRight" data-wow-duration="2s">
                            <div class="section-title">
                                <h3><span></span>Soluciones de relación con clientes</h2>
                            </div>
                                <div class="flexible-price-single-item wow animate__animated animate__zoomIn" data-wow-duration="1s">
                                    <ul class="flexible-price-lsit">
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Take Away.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Delivery.</li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Reservas.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Fidelización.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Carta Digital.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Pedidos en mesa.</p></li>
                                        <li><p><i class="fa fa-check-square" aria-hidden="true"></i> Pago en mesa.</p></li>
                                    </ul>
                                </div>
                        </div>
                        </div>
                    </div>
                    
                </div> 
            </div>
        </div>
            </div>
    </section>
    <!--  team area end -->

    <!-- contact area -->
    <section id="contact" class="contact-area">
        <div class="container">
            <div class="row contact-area-row wow animate__animated animate__zoomIn" data-wow-duration="1.5s">
                <div class="col-lg-10 contact-form-col">
                    <input type="hidden" id="base_url" value="{{url('/')}}">
                    @if ($notify = session('notify'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        </button>
                        <strong>Información: </strong> {{ $notify['msg'] }}
                      </div>
                    @endif
                    <form id="contact-form" class="atf-contact-form" method="POST" action="php/mail.php">
                        <div id="loading-screen" style="display: none;"> 
                            <div class="spinner-border" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </div>    
                        @csrf
                        <h3>Solicita Información - <a href="tel:+34977552258">977 55 22 58</a></h3>
                        <div >
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="name-input" type="text" id="name" name="name" placeholder="Nombre empresa*" required>
                                    <div id="nameFeedback" class="invalid-feedback pl-1"></div>
                                </div>
                                <div class="col-md-6">
                                    <input class="email-input" type="tex"  id="responsable" name="responsable" placeholder="Responsable*" required>
                                     <div id="responsableFeedback" class="invalid-feedback pl-1"></div>
                                </div>
                            </div>
                        </div>    
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input class="name-input " type="text" placeholder="Teléfono contacto*" name="phone" id="phone" required>
                                    <div id="phoneFeedback" class="invalid-feedback pl-1"></div>
                                </div>
                                <div class="col-md-6">
                                    <input class="email-input" type="email" placeholder="Correo Electrónico*" name="email" id="email" required>
                                    <div id="emailFeedback" class="invalid-feedback pl-1"></div>
                                </div>
                            </div>
                        </div>    
                         <div>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea placeholder="Mensaje" name="message" id="message" required></textarea>
                                    <div id="messageFeedback" class="invalid-feedback pl-1"></div>
                                </div>
                            </div>
                        </div>    
                        <button  type='button' onclick="formulario()" class="button-btn">E N V I A R</button>

                    </form>
                   
                    <p class="form-message"></p>
                </div>
            </div>
        </div>
    </section>
    <!-- contact area -->

    <!-- footer area start -->
    <section class="footer-area">
        <div class="container">
            <div class="row footer-row">
                <div class="col-lg-4 col-md-6 footer-item-col-1 footer-item-col">
                    <div class="single-footer-item">
                        <div class="footer-content">
                            <div class="footer-logo">
                                <a href="#"><img src="images/logo_footer.png" alt="footer logo"></a>
                            </div>
                           >
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 footer-item-col-2 footer-item-col">
                    <div class="single-footer-item">
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 footer-item-col-3 footer-item-col">
                    <div class="single-footer-item">
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 footer-item-col-4 footer-item-col">
                    <div class="single-footer-item">
                        <h4>Contacto</h4>
                        <ul class="footer-contact-info">
                            <li><a href="tel:+34977552258">+34 977 55 22 58</a></li>
                            <li><a href="mailto:contacto@tecnobravo.com">contacto@tecnobravo.com</a></li>
                            <li><a href="https://g.page/TecnoBravo?share">C/ Cinco, 55 Local 3 Bonavista 43100, Tarragona</a></li>
                        </ul>
                        <ul class="footer-social-area">
                            <li><a href="https://www.facebook.com/TecnoBravo-208447529190465/"><i class="fab fa-facebook-f"></i>acebook</a></li>
                            <li><a href="https://www.instagram.com/tecnobravo_tb/"><i class="fab fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-12 copyright-content-col">
                        <p>&#169; Copyright 2022 , All rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- footer area end -->

    <!-- js link -->

    <!-- jquery link -->
    <script src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
    <!-- sticky js -->
    <script src="{{ asset('js/jquery.sticky.js') }}"></script>
    <!-- slicknav js -->
    <script src="{{ asset('js/jquery.slicknav.min.js') }}"></script>
    <!-- carousel js -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- popper js -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!-- bootstrap min js -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- wow js -->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!-- ajaxchimp js -->
    <script src="{{ asset('js/ajaxchimp.min.js') }}"></script>
    <!-- form-contact js -->
    <script src="{{ asset('js/form-contact.js') }}"></script>
    <!-- main js -->
    <script src="{{ asset('js/main.js') }}"></script>
    <!-- js link -->
    <script src="{{ asset('js/contacto.js') }}"></script>
    <!-- js link -->
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <!-- js link -->

</body>

</html>
